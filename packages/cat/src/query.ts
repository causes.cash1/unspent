import axios from 'axios';
import { COINGECKO_CHART, CHAINGRAPH } from './config.js';

function sleep(ms: number) {
  return new Promise(resolve => setTimeout(resolve, ms));
}



export async function getBlockHistory(start: number, end: number) {

  let resp = await getBlockTimestamps(
    CHAINGRAPH,
    start,
    end
  )

  await sleep(1000);
  return resp
}


export async function getBlockTimestamps(host: string, start: number, end: number) {
  const query = `
  query GetBlockTimestamps($start:bigint, $end:bigint) {
    block(
      where: {
        _and: [
          { height: { _gt: $start } }
          { height: { _lte: $end } }
          { accepted_by: { node: { name: { _eq: "bchn-mainnet" } } } }
        ]
      }
    ) {
      height
      timestamp
    }
  }
  `
  const response = await axios({
    url: host,
    method: "post",
    data: {
      query: query,
      variables: {
        start: start,
        end: end
      },
    },
  }).catch((e: any) => {
    throw e;
  });

  // raise errors from chaingraph
  if (response.data.error || response.data.errors) {
    if (response.data.error) {
      throw Error(response.data.error);
    } else {
      throw Error(response.data.errors[0].message);
    }
  }

  // TODO cleanup response
  return response.data.data.block.map((x: any) => {
    return {
      timestamp: new Date(parseInt(x.timestamp) * 1000),
      height: parseInt(x.height)
    }
  });
}

export async function getOutputs(lockingBytecode: string) {
  return (await getOutputsRaw(CHAINGRAPH,
    lockingBytecode,
    0,
    50
  )).flat()
}

export async function getOutputsRaw(host: string, lockingBytecode: string, offset: number, limit: number) {
  const query = `
  query GetTransactionHistory(
    $lockingBytecode: String!
    $limit: Int
    $offset: Int
  ) {
      search_output_prefix(
        args: { locking_bytecode_prefix_hex: $lockingBytecode }
      limit: $limit
      offset: $offset
      where: {
        _or: [
          {
            transaction: {
              block_inclusions: {
                block: {
                  accepted_by: { node: { name: { _regex: "bchn-mainnet" } } }
                }
              }
            }
          }
        ]
      }
      order_by: { transaction: { internal_id: desc } }
    ) {
      transaction {
        hash
        outputs{
          output_index
          value_satoshis
          locking_bytecode
        }
        inputs{
          value_satoshis
          input_index
          unlocking_bytecode
          outpoint{
            locking_bytecode
          }
        }
        block_inclusions {
          block {
            height
          }
        }
      }
    }
  }      
  
  `
  const response = await axios({
    url: host,
    method: "post",
    data: {
      query: query,
      variables: {
        limit: limit,
        offset: offset,
        lockingBytecode: lockingBytecode.substring(0, 44)
      },
    },
  }).catch((e: any) => {
    throw e;
  });

  // raise errors from chaingraph
  if (response.data.error || response.data.errors) {
    if (response.data.error) {
      throw Error(response.data.error);
    } else {
      throw Error(response.data.errors[0].message);
    }
  }

  // TODO cleanup response
  let matchingOutputs = response.data.data.search_output_prefix.map((tx: any) => {
    let outputs = tx.transaction.outputs.filter((o: any) => o.locking_bytecode.includes(lockingBytecode) > 0)
    let inputs = tx.transaction.inputs.filter((i: any) => i.outpoint.locking_bytecode.includes(lockingBytecode) > 0)
    return [
      ...outputs.map((output: any) => {
        return {
          id: tx.transaction.hash.substring(3) + ":o:" + output.output_index,
          value: -parseInt(output.value_satoshis),
          height: parseInt(tx.transaction.block_inclusions[0].block.height),
          locking_bytecode: lockingBytecode
        }
      }),
      ...inputs.map((input: any) => {
        return {
          id: tx.transaction.hash.substring(3) + ":i:" + input.input_index,
          value: parseInt(input.value_satoshis),
          height: parseInt(tx.transaction.block_inclusions[0].block.height),
          locking_bytecode: lockingBytecode
        }
      })
    ]
  }
  )

  return matchingOutputs


}


// https://api.coingecko.com/api/v3/coins/bitcoin-cash/market_chart/range?vs_currency=usd&from=1501546841&to=
// 1705191932803
// 1705191641&precision=2
export async function getPriceHistory() {

  const response = await axios.get(COINGECKO_CHART, {
    params: {
      vs_currency: "usd",
      from: "1501546841",
      to: Date.now() / 1000

    }
  }).catch((e: any) => {
    throw e;
  });

  return response.data.prices.map((x: any) => {
    return {
      timestamp: new Date(x[0]),
      value: x[1]
    }
  });
}